<?php



Auth::routes();
Route::view('/admin','admin')->name('admin')->middleware('auth');;
Route::get('/', 'HomeController@index')->name('home');
Route::resource('pelicula','PeliculaController')->middleware('auth');;
Route::resource('banner','BannerController')->middleware('auth');;
Route::resource('user','UsersController');
Route::resource('horario','HorarioController');
Route::resource('cartelera','CarteleraController');
Route::get('/image/file/{filename}', 'ShowImage')->name('image');

Route::get('pdf/{option}','PdfController@generatePDF2');

Route::get('pdf','PdfController@generatePDF');
Route::post('pdf','PdfController@obtenerFecha')->name('fecha');
Route::get('pdf/{from}/{to}','PdfController@generateFecha');
Route::get('/reporte','ReporteController@index')->name('reporte');
