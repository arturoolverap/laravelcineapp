<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetallesTable extends Migration{
   
    public function up()
    {
        Schema::create('detalles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('director');
            $table->string('actores');
            $table->string('sinopsis');
            $table->string('trailer');
            $table->timestamps();
        });
    }

  
    public function down()
    {
        Schema::dropIfExists('detalles');
    }
}
