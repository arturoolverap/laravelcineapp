<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHorariosTable extends Migration{
    
    public function up(){
        Schema::create('horarios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('fecha');
            $table->string('hora');
            $table->string('sala', 100);
            $table->double('precio', 8, 2);
            $table->unsignedBigInteger('pelicula_id');
            $table->foreign('pelicula_id')->references('id')->on('peliculas');
            $table->timestamps();
        });
    }

   
    public function down()
    {
        Schema::dropIfExists('horarios');
    }
}
