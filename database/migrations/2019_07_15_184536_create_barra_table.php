<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBarraTable extends Migration{
   
    public function up()
    {
        Schema::create('banners', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('titulo');
            $table->string('archivo');
            $table->enum('estatus', ['Activo','Inactivo']);
            $table->timestamps();
        });
    }

   
    public function down()
    {
        Schema::dropIfExists('banners');
    }
}
