<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNoticiasTable extends Migration{
    
    public function up(){
        Schema::create('noticias', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('titulo');
            $table->date('fecha');
            $table->text('detalle');
            $table->enum('estatus', ['Activo','Inactivo']);
            $table->timestamps();
        });
    }

   
    public function down(){
        Schema::dropIfExists('noticias');
    }
}
