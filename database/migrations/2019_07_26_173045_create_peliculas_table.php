<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeliculasTable extends Migration{
   
    public function up() {
        Schema::create('peliculas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('titulo');
            $table->string('duracion');
            $table->enum('clasificacion', ['AA','A','B','B15','C','D']);
            $table->string('genero');
            $table->string('imagen');
            $table->date('fechaEstreno');
            $table->enum('estatus', ['Activo','Inactivo']);
            $table->unsignedBigInteger('detalles_id');
            $table->foreign('detalles_id')->references('id')->on('detalles');
            $table->timestamps();
        });
    }

   
    public function down(){
        Schema::dropIfExists('peliculas');
    }
}
