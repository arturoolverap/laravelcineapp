<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder{
    
    public function run(){
       DB::table('roles')->insert([
            'name' => 'admin',
            'display_name' =>'Administrador'
        ]);
       DB::table('roles')->insert([
            'name' => 'mod',
            'display_name' =>'Moderador'
        ]);

    }
}
