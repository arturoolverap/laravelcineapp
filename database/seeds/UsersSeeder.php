<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
class UsersSeeder extends Seeder{
    public function run(){
       DB::table('users')->insert([
            'name' => 'Arturo',
            'email' => 'arturoolverap@gmail.com',
            'username' => 'arturoolverap',
            'password' => Hash::make('12345678'),
            'role_id' => 1,
        ]);
       

    }
}
