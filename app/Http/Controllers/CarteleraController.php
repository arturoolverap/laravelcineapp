<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pelicula;
use App\Horario;
class CarteleraController extends Controller
{
   
    public function index(){
       $peliculas = Pelicula::orderBy('fechaEstreno','asc')->paginate(10);      
        
        return view('cartelera.home',['peliculas'=>$peliculas]);
    }

   
    public function create(){
        return redirect('banner');
    }

    
    public function store(Request $request){
        
    }

   
    public function show($id){
        $pelicula = Pelicula::find($id);
        $horarios = Horario::where('pelicula_id', $id)->get();
        return view('cartelera.show',['pelicula'=>$pelicula,'horarios'=>$horarios]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
