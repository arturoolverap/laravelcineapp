<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
class UsersController extends Controller{
    public function __construct()
    {
        $this->middleware('auth');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }
    
    public function index(){
        $users = User::orderBy('id','asc')->get();       
        return view('user.home',['users'=>$users]);
    }

    
    public function create() {
        $roles = Role::orderBy('id','asc')->get();       
      
       return view('user.formulario',['roles'=>$roles]);
    }

   
    public function store(Request $request) {
        $destino = $request['email'];
        $nombre = $request['name'];
        $mensaje = $request['password'];
        $data1 = [
            'mensaje' => $mensaje,
            'destino' => $destino,
            'nombre' => $nombre
        ];
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://mailevaluacion.000webhostapp.com/datos.php",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 20,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $data1,
            CURLOPT_HTTPHEADER => array(
                // Set here requred headers
                "accept: */*",
                "accept-language: en-US,en;q=0.8",
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
        ///
        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'username' => $request['username'],
            'password' => Hash::make($request['password']),
            'role_id' => $request['role'],
            ]);
            $users = User::orderBy('id','asc')->get();       
        return view('user.home',['users'=>$users]);  
        }
        
               
    }

    
    public function show($id) {
        //
    }

    
    public function edit($id) {
        $user = User::findOrFail($id);
        $roles = Role::orderBy('id','asc')->get();  
       return view('user.edit',['user'=>$user,'roles'=>$roles]);
    }

    
    public function update(Request $request, $id) {
        $user = User::find($id);
        $user->name = $request['name'];
        $user->email = $request['email'];
        $user->username = $request['username'];
        $user->password = Hash::make($request['password']);
        $user->role_id = $request['role'];
        $user->save();
        return redirect('user');
    }

    
    public function destroy($id) {
        $user = User::find($id);
        $user->delete();
        return redirect('user');
    }

    
}
