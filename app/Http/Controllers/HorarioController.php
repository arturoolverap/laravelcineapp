<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Horario;
use App\Pelicula;
class HorarioController extends Controller{
   
    public function index(){
        $horarios = Horario::orderBy('id','asc')->paginate(10); 
        return view('horarios.home',['horarios'=>$horarios]);
    }

    
    public function create(){
       $peliculas = Pelicula::orderBy('id','asc')->get();      
        
        return view('horarios.formulario',['peliculas'=>$peliculas]);
    }

   
    public function store(Request $request){
        $validate  = $this->validate($request,[
            'fecha'=> 'required',
            'hora' => 'required',
            'sala'=> 'required',
            'precio'=> 'required',
        ]);
        $fecha = $request->input('fecha');
        $hora = $request->input('hora');
        $sala = $request->input('sala');
        $precio = $request->input('precio');
        $peliculaId = $request->input('pelicula');

        $horario = new Horario();
        $horario->fecha = $fecha;
        $horario->hora = $hora;
        $horario->sala = $sala;
        $horario->precio = $precio;
        $horario->pelicula_id = $peliculaId;
        //var_dump($horario->precio);
        $horario->save();
       
       // return redirect('horario/create');
    }

   
    public function show($id)
    {
        //
    }

   
    public function edit($id){
        $horario = Horario::findOrFail($id);
        $peliculas = Pelicula::orderBy('id','asc')->get();      
        
        return view('horarios.edit',['peliculas'=>$peliculas,'horario'=>$horario]);
    }


    public function update(Request $request, $id){
        $validate  = $this->validate($request,[
            'fecha'=> 'required',
            'hora' => 'required',
            'sala'=> 'required',
            'precio'=> 'required',
        ]);
        $horario = Horario::find($id);
        $horario->fecha = $request->input('fecha');
        $horario->hora = $request->input('hora');
        $horario->sala = $request->input('sala');
        $horario->precio = $request->input('precio');
        
        $horario->save();
       
       return redirect('horario');
    }


    public function destroy($id){
        $horario = Horario::find($id);
        $horario->delete();
        return redirect('horario');
    }
}