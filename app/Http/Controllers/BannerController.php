<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\Banner;
use Illuminate\Pagination\LengthAwarePaginator;
class BannerController extends Controller{
    
    public function index(Request $request){
        $banners = Banner::orderBy('id','asc')->paginate(1);       
        return view('banner.home',['banners'=>$banners]);
    }

    
    public function create(){
        return view('banner.formulario');
    }

   
    public function store(Request $request){
       // Validacion
        $validate  = $this->validate($request,[
            'titulo'=> 'required',
            'imagen' => 'required|image'
        ]);


        $image = $request->file('imagen');
        $titulo = $request->input('titulo');
        $estatus = $request->input('estatus');
       
        $imageName = time().$image->getClientOriginalName();
        var_dump($imageName);
        var_dump($image);

        //Subir al servidor
        $banner = new Banner();
        $banner->titulo =$titulo;
        $banner->estatus = $estatus;

       
        if ($image) {
           // Storage::disk('images')->put($imageName,File::get($image));
            $request->imagen->storeAs('public/images',$imageName);
            $banner->archivo = $imageName;
        }
        $banner->save();
        return redirect('banner');
        
    }


    
    public function show($id){
        //
    }

    
    public function edit($id)
    {
        //
    }

 
    public function update(Request $request, $id){
        $banner = Banner::find($id);
        if($banner->estatus == "Activo"){
            $banner->estatus = "Inactivo";
        }else{
            $banner->estatus = "Activo";
        }
        $banner->save();
         
        return redirect('banner');
    }

    
    public function destroy($id)
    {
        //
    }
}
