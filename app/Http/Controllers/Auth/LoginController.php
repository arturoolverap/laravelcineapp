<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
class LoginController extends Controller
{
    

    use AuthenticatesUsers;

   
    protected $redirectTo = '/';
   
    
   
    public function __construct(){
       
        $this->middleware("throttle:22,2")->except('logout');
    }
    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            $this->username() => 'required', 
            'password' => 'required',
            'g-recaptcha-response' => 'required|captcha'
        ]);
    }
}
