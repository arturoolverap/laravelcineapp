<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pelicula;
use App\Detalles;
class PeliculaController extends Controller
{
    
    public function index(){
        $peliculas = Pelicula::orderBy('id','asc')->paginate(1);      
        
        return view('pelicula.home',['peliculas'=>$peliculas]);
        
    }

    
    public function create(){
        return view('pelicula.formulario');
    }

   
    public function store(Request $request){
        // Validacion
        $validate  = $this->validate($request,[
            'titulo'=> 'required',
            'imagen' => 'required|image',
            'duracion'=> 'required|regex:^[0-9]+^',
            'clasificacion'=> 'required',
            'fechaEstreno'=> 'required',
        ]);
        ///Pelicula Aqui :v
        $image = $request->file('imagen');
        $titulo = $request->input('titulo');
        $duracion = $request->input('duracion');
        $clasificacion = $request->input('clasificacion');
        $genero = $request->input('genero');
        $estatus = $request->input('estatus');
        $fechaEstreno = $request->input('fechaEstreno');
        // Detalles
        $director = $request->input('director');   
        $actores = $request->input('actores'); 
        $sinopsis = $request->input('sinopsis');   
        $trailer = $request->input('trailer');
        // Save 
        $detalles = new Detalles();
        $detalles->director = $director;
        $detalles->actores = $actores;
        $detalles->sinopsis = $sinopsis;
        $detalles->trailer = $trailer;
        $detalles->save();
        ///
        $imageName = time().$image->getClientOriginalName();
        var_dump($imageName);
        var_dump($image);

        //Subir al servidor
        $pelicula = new Pelicula();
        $pelicula->titulo =$titulo;
        $pelicula->duracion = $duracion;
        $pelicula->clasificacion =$clasificacion;
        $pelicula->genero = $genero;
        $pelicula->estatus = $estatus;
        $pelicula->fechaEstreno =$fechaEstreno;
        $pelicula->detalles_id = $detalles->id;
       
        if ($image) {
           // Storage::disk('images')->put($imageName,File::get($image));
            $request->imagen->storeAs('public/images',$imageName);
            $pelicula->imagen = $imageName;
        }
        $pelicula->save();
        return redirect('pelicula');
    }

    
    public function show($id){
        //
    }

    
    public function edit($id){
        $pelicula = Pelicula::findOrFail($id);
        return view('pelicula.edit',['pelicula'=>$pelicula]);
    }

    
    public function update(Request $request, $id){
        $titulo = $request->input('titulo');
        $duracion = $request->input('duracion');
        $clasificacion = $request->input('clasificacion');
        $genero = $request->input('genero');
        $estatus = $request->input('estatus');
        $fechaEstreno = $request->input('fechaEstreno');
        // Detalles
        $director = $request->input('director');   
        $actores = $request->input('actores'); 
        $sinopsis = $request->input('sinopsis');   
        $trailer = $request->input('trailer');

        $detalles = Detalles::find($id);
        $detalles->director = $director;
        $detalles->actores = $actores;
        $detalles->sinopsis = $sinopsis;
        $detalles->trailer = $trailer;
        $detalles->save();

        $pelicula = Pelicula::find($id);
        $pelicula->titulo =$titulo;
        $pelicula->duracion = $duracion;
        $pelicula->clasificacion =$clasificacion;
        $pelicula->genero = $genero;
        $pelicula->estatus = $estatus;
        $pelicula->fechaEstreno =$fechaEstreno;
        $pelicula->detalles_id = $detalles->id;
        $pelicula->save();

        return redirect('pelicula');
    }

   
    public function destroy($id){
        $pelicula = Pelicula::find($id);
        if($pelicula->estatus == "Activo"){
            $pelicula->estatus = "Inactivo";
        }else{
            $pelicula->estatus = "Activo";
        }
        $pelicula->save();
         
        return redirect('pelicula');
    }
}
