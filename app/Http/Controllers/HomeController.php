<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Banner;
use App\Pelicula;

class HomeController extends Controller{
    
    public function index(Request $request){
    	$banners = Banner::orderBy('id','asc')->where('estatus', 'Activo')->get(); 
    	$peliculas = Pelicula::orderBy('id','asc')->where('estatus', 'Activo')->get(); 
    	 	  
        return view('home',['banners'=>$banners,'peliculas' => $peliculas]);
    }

   
}
