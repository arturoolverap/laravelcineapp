<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pelicula;
use App\User;
use PDF;
class pdfController extends Controller{
    
     public function generatePDF(){
     	$now = date("d-m-Y");
     	$from = date("d-m-Y",strtotime($now."- 1 month"));
		$to = date("d-m-Y");

        echo "From ";
        var_dump($from);
        echo "<br>";
        var_dump($now);
     	$peliculas = Pelicula::whereBetween('fechaEstreno', [$from, $to])->get();
     	
        $data = ['title' => 'CineApp','peliculas' => $peliculas];
        $pdf = PDF::loadView('myPDF', $data);
        return $pdf->download('reporte.pdf');
    }

    public function generatePDF2($id){
        switch ($id) {
            case '1':
                $peliculas = Pelicula::orderBy('id','asc')->get();  
                $data = ['title' => 'CineApp','peliculas' => $peliculas];
                $pdf = PDF::loadView('myPDF', $data);
               
                break;
            case '2':
                $now = date("d-m-Y");
                $from = date("d-m-Y",strtotime($now."- 1 month"));
                $to = date("d-m-Y");
                $peliculas = Pelicula::whereBetween('fechaEstreno', [$from, $to])->get();
                $data = ['title' => 'CineApp','peliculas' => $peliculas];
                $pdf = PDF::loadView('myPDF', $data);
              
                break;
            case '3':
                $usuarios = User::orderBy('id','asc')->get();  
                $data = ['title' => 'CineApp','usuarios' => $usuarios];
                $pdf = PDF::loadView('userReport', $data);
               
                break;

        }
         return $pdf->download('reporte.pdf');
        
    }
    public function generateFecha($from,$to){        
        $peliculas = Pelicula::whereBetween('fechaEstreno', [$from, $to])->get();
        $data = ['title' => 'CineApp','peliculas' => $peliculas];
        $pdf = PDF::loadView('myPDF', $data);
        return $pdf->download('reporte.pdf');
    }
    public function obtenerFecha(Request $request){
         $validate  = $this->validate($request,[
            'fechaFrom'=> 'required',
            'fechaTo' => 'required'
        ]);
         $from = $request->input('fechaFrom');
         $to = $request->input('fechaTo');
         var_dump($from);
         echo "<br>";
         var_dump($to);
        $peliculas = Pelicula::whereBetween('fechaEstreno', [$from, $to])->get();
        $data = ['title' => 'CineApp','peliculas' => $peliculas];
        $pdf = PDF::loadView('myPDF', $data);
        return $pdf->download('reporte.pdf');
    }
}
