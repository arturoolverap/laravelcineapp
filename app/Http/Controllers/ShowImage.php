<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class ShowImage extends Controller{
    
    public function __invoke($filename){
       $file = Storage::disk('images')->get($filename);
        return new Response($file,200);
      //  echo $filename;
    }
}
