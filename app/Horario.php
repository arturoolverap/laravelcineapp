<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Horario extends Model{
    protected $table = 'horarios';

    protected $fillable = [
        'fecha','hora','sala','precio','pelicula_id'
    ];

    public function pelicula(){
        return $this->belongsTo(Pelicula::class);
    }
}
