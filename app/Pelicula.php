<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pelicula extends Model{
     protected $table = 'peliculas';

    protected $fillable = [
        'titulo','duracion','clasificacion','genero','imagen','fechaEstreno','estatus','detalles_id'
    ];

    public function detalles(){
        return $this->belongsTo(Detalles::class);
    }
}


