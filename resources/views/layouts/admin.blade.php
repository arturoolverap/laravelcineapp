<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

   

    <!-- Fonts -->
    <link href="{{ asset('css/nunito.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/font.css') }}" />
  <link rel="stylesheet" href="{{ asset('css/icons.css') }}">
  <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <!-- Styles -->
    <link href="{{ asset('css/material-dashboard.css?v=2.1.1') }}" rel="stylesheet">

</head>
<body>
    
        <div class="wrapper ">
    <div class="sidebar" data-color="purple" data-background-color="white">
    
      <div class="logo">
        <a class=" px-4 navbar-brand" href="{{ url('/') }}">
            <img  src="{{ asset('assets/film-reel2.png') }}" alt=""> {{ config('app.name', 'Laravel') }}
        </a>
        
      </div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <li class="nav-item active  ">
            <a class="nav-link" href="">
              <i class="material-icons">dashboard</i>
              <p>Dashboard</p>
            </a>
          </li>
          @if(Auth::user()->role->name == "admin")
           <li class="nav-item  ">
            <a class="nav-link" href="{{ url('/user') }}">
              <i class="material-icons">person</i>
              <p>Usuarios</p>
            </a>
          </li>
          <li class="nav-item  ">
            <a class="nav-link" href="{{ url('/reporte') }}">
              <i class="material-icons">description</i>
              <p>Reportes</p>
            </a>
          </li>
          @endif
           <li class="nav-item  ">
            <a class="nav-link" href="{{ url('/banner') }}">
              <i class="material-icons">perm_media</i>
              <p>Banner</p>
            </a>
          </li>
           <li class="nav-item  ">
            <a class="nav-link" href="{{ url('/pelicula') }}">
              <i class="material-icons">camera_roll</i>
              <p>Peliculas</p>
            </a>
          </li>
         
          <li class="nav-item  ">
            <a class="nav-link" href="{{ url('/horario') }}">
              <i class="material-icons">date_range</i>
              <p>Horarios</p>
            </a>
          </li>
          <!-- your sidebar here -->
        </ul>
      </div>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
     <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <a class="navbar-brand" href="#pablo">Banner</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
            
            <ul class="navbar-nav">
              
             
              <li class="nav-item dropdown">
                <a class="nav-link" href="" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  {{ Auth::user()->name }} 
                  <i class="material-icons">person</i>
                  <p class="d-lg-none d-md-block">
                    
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  
                  <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                  </a>

                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      @csrf
                  </form>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <main class="py-1" >
             <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
            @yield('content')
        </main>
        </div>
      </div>
      
    </div>
  </div>
        
    </div>
</body>
</html>
