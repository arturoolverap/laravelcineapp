<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>@yield('title','CineApp')</title>
</head>
<body>

	<nav>
		<ul>
			<li><a href="/">home</a></li>
			<li><a href="/peliculas">peliculas</a></li>
			<li><a href="/cartelera">cartelera</a></li>
		</ul>
	</nav>
	@yield('content')
</body>
</html>