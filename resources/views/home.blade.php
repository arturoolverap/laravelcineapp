@extends('layouts.app')

@section('content')

<div class="">
		<div class="row">
			<div class="col-sm">
			<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
     @foreach($banners as $banner)     
         @if ($loop->index == 0)
             <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
         @else
             <li data-target="#carouselExampleIndicators" data-slide-to="{{$loop->index}}"></li>
        @endif

      
     @endforeach
   
  </ol>
  <div class="carousel-inner">
    
     @foreach($banners as $banner)
      
      @if ($loop->index == 0)
        <div class="carousel-item active">
          <img class="d-block w-100" src="{{route('image',['filename'=>$banner->archivo])}}">
        </div>
      @else
        <div class="carousel-item">
          <img class="d-block w-100" src="{{route('image',['filename'=>$banner->archivo])}}">
        </div>
      @endif
    
    @endforeach
   
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>		</div>
		</div>
	</div>


<script>
	
	$('.carousel').carousel({
  interval: 2000
});


	

</script>


	<div class="container py-4">
    <div class="row">
      @foreach($peliculas as $pelicula)
        <div class="col-md-4 col-sm-6 col-lg-3">
          
          <div class="card" style="width:70%; margin:14px;">
            <a href="{{ route('cartelera.show',$pelicula->id) }}">
            <img class="card-img-top" src="{{route('image',['filename'=>$pelicula->imagen])}}" style="width:100%" alt="">
            </a>
            <div class="card-body">
              <p class="card-title">{{$pelicula->titulo}}</p>
              <p class="card-text">
                  <span >{{$pelicula->clasificacion}}</span>
                  <span >{{$pelicula->duracion}} min.</span>
                  <span >{{$pelicula->genero}}</span>
              </p>
         
            </div>
          </div>

         
        </div>  
        @endforeach
      
      
  </div>
</div>
@endsection
