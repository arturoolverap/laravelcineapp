@extends('layouts.admin')

@section('content')

  

    <div class="row">
      <div class="col-lg-3 col-md-6 col-sm-6">
           <div class="card card-stats">
                <div class="card-header card-header-success card-header-icon">
                  <div class="card-icon">
                    <a  style="color:white"  href="{{ url('/pdf/1') }}">
                    <i class="material-icons">add</i>
                    </a>
                  </div>
                  <p class="card-category">Todas peliculas</p>
                  <h3 class="card-title">Reporte</h3>
                  
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons"></i> 
                    <p>Todas las peliculas en la historia</p>
                  </div>
                </div>
              </div>

        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
           <div class="card card-stats">
                <div class="card-header card-header-success card-header-icon">
                  <div class="card-icon">
                    <a  style="color:white"  href="{{ url('/pdf/2') }}">
                    <i class="material-icons">add</i>
                    </a>
                  </div>
                  <p class="card-category">Estrenos mes</p>
                  <h3 class="card-title">Reporte</h3>
                  
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons"></i> 
                    <p>Los estrenos de este mes</p>
                  </div>
                </div>
              </div>
              
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
           <div class="card card-stats">
                <div class="card-header card-header-success card-header-icon">
                  <div class="card-icon">
                    <a  style="color:white"  href="{{ url('/pdf/3') }}">
                    <i class="material-icons">add</i>
                    </a>
                  </div>
                  <p class="card-category">Empleados</p>
                  <h3 class="card-title">Reporte</h3>
                  
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons"></i> 
                    <p>Todos los empleados</p>
                  </div>
                </div>
              </div>
              
        </div>
    </div>
   
    <div class="row">
      <div class="col-md">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Reportes</h4>
                  <p class="card-category">Reportes peliculas estreno por fecha</p>
                </div>
                <div class="card-body">
                  <form method="POST" action="{{ route('fecha') }}" >
                    @csrf
                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="bmd-label-floating">Fecha desde</label>
                          <input type="date" name="fechaFrom"  required class="form-control">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="bmd-label-floating">Fecha hasta</label>
                          <input type="date" name="fechaTo" required class="form-control">
                        </div>
                      </div>
                    </div>
                    

                  
                    
                    <button type="submit" class="btn btn-primary pull-right">Genera</button>
                    <div class="clearfix"></div>
                  </form>
                </div>

            
        </div>
    </div>
    </div>
    
@endsection
