<!DOCTYPE html>
<head>
	<title>Hi</title>
</head>

<body>

	<h1>Reporte - {{ $title }}</h1>

	<table>
		<tr>
        <td>ID</td>
        <td>Titulo</td>
        <td>Duración</td>
        <td>Clasificación</td>
        <td>Genero</td>
        <td>Estatus</td>
        <td>Fecha Estreno</td>
        	
      </tr>
     

	@foreach($peliculas as $pelicula)
		 <tr>
			<td>{{$pelicula->id}}</td>
			<td>{{$pelicula->titulo}}</td>
			<td>{{$pelicula->duracion}}</td>
			<td>{{$pelicula->clasificacion}}</td>
			<td>{{$pelicula->genero}}</td>
			
			<td>{{$pelicula->estatus}}</td>
			<td>{{$pelicula->fechaEstreno}}</td>
          
        </tr>
	@endforeach
	
	</table>
</body>

</html>