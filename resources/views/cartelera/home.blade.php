@extends('layouts.app')

@section('content')

  

    <div class="container">
            <div class="container py-4">
              <div class="row">
                @foreach($peliculas as $pelicula)
                  <div class="col-md-4 col-sm-6 col-lg-3">
                    
                    <div class="card" style="width:70%; margin:14px;">
                      <a href="{{ route('cartelera.show',$pelicula->id) }}">
                      <img class="card-img-top" src="{{route('image',['filename'=>$pelicula->imagen])}}" alt="">
                      </a>
                      <div class="card-body">
                        <p class="card-title">{{$pelicula->titulo}}</p>
                        <p class="card-text">
                            <span >{{$pelicula->clasificacion}}</span>
                            <span >{{$pelicula->duracion}} min.</span>
                            <span >{{$pelicula->genero}}</span>
                        </p>
                   
                      </div>
                    </div>

                   
                  </div>  
                  @endforeach
                
                
            </div>
          </div>
            <!-- Paginacion -->
            <div class="row ">
              <div class="col-md-5 offset-md-4">
                <div class="clearfix">
                {{$peliculas->links()}}
              </div>
              </div>
            </div>
    
    </div>
    
@endsection
