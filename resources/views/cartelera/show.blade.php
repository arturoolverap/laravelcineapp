@extends('layouts.app')

@section('content')
   <div class="container">
      <div class="">

          <h2>{{strtoupper($pelicula->titulo)}}</h2>
          <hr/>
      </div>
      <div class="row">
          <div class="col-sm-3">
            <p class="text-center">
              <img class="img-rounded" src="{{route('image',['filename'=>$pelicula->imagen])}}" alt="Generic placeholder image" width="155" height="220">            
            </p>
          </div>
          <div class="col-sm-9">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">DETALLES</h3>
              </div>
              <div class="panel-body">                           
                <p>
                  Título Original : {{$pelicula->titulo}} <br>
                  Actores : {{$pelicula->detalles->actores}} <br>
                  Director: {{$pelicula->detalles->director}} <br>                  
                  Clasificación: {{$pelicula->clasificacion}} <br>
                  Duración: {{$pelicula->duracion}} minutos <br>
                  Género: {{$pelicula->genero}} <br>                  
                  Fecha Estreno: {{$pelicula->fechaEsteno}}                  
                </p> 

              </div>
            </div>                          
          </div>
        </div>
        <!--Horario aqui --> 
        <br>  
        <h4>Horarios</h4>
        <div class="row">
          <table class="table table-striped">
              <thead>
                <tr>                  
                  <th>Hora</th>
                  <th>Sala</th>                                  
                  <th>Precio</th>                                  
                </tr>
              </thead>
              <tbody>             
                @foreach($horarios as $horario)       
                <tr>                 
                  <td>{{$horario->hora}}</td>
                  <td>{{$horario->sala}}</td>  
                  <td>${{$horario->precio}}</td>  
                </tr>
                @endforeach              
              </tbody>           
            </table>
        </div>

        <div class="row">
          <div class="col-sm-7">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">Trailer</h3>
              </div>
              <div class="panel-body">
                <iframe width="100%" height="315" 
                        src="{{$pelicula->detalles->trailer}}" >                          
                </iframe>
              </div>
            </div>           
          </div> 
          <div class="col-sm-5">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">SINOPSIS</h3>
              </div>
              <div class="panel-body">
                <p>{{$pelicula->detalles->sinopsis}}</p>
              </div>
            </div>                          
          </div>
        </div>
   </div>

  
   
    
@endsection
