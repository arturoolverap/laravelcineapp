@extends('layouts.admin')

@section('content')

  

    <div class="row">
      <div class="col-lg-3 col-md-6 col-sm-6">
           <div class="card card-stats">
                <div class="card-header card-header-success card-header-icon">
                  <div class="card-icon">
                    <a  style="color:white"  href="{{ url('/pelicula/create') }}">
                    <i class="material-icons">add</i>
                    </a>
                  </div>
                  <p class="card-category">Pelicula</p>
                  <h3 class="card-title">Nuevo</h3>
                  
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons"></i> 
                  </div>
                </div>
              </div>
             </div>

            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title ">Pelicula</h4>
                  <p class="card-category"> Here is a subtitle for this table</p>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table">
                      <thead class=" text-primary">
                        <th>ID</th>
                        <th>Titulo</th>
                        <th>Duración</th>
                        <th>Clasificación</th>
                        <th>Genero</th>
                        <th>Imagen</th>
                        <th>Estatus</th>
                        <th>Fecha Estreno</th>
                        <th>Opciones</th>
                      </thead>
                      <tbody>

                        @foreach($peliculas as $pelicula)
                        <tr>
                          <td>{{$pelicula->id}}</td>
                          <td>{{$pelicula->titulo}}</td>
                           <td>{{$pelicula->duracion}}</td>
                            <td>{{$pelicula->clasificacion}}</td>
                             <td>{{$pelicula->genero}}</td>
                          <td><img class="imagen" src="{{route('image',['filename'=>$pelicula->imagen])}}"></td>
                          <td>{{$pelicula->estatus}}</td>
                          <td>{{$pelicula->fechaEstreno}}</td>
                          <td class="td-actions text-left">
                             <form  method="get" action="{{ route('pelicula.edit',$pelicula->id) }}">
                                
                                <button type="submit" rel="tooltip" title="Edit Task" class="btn btn-primary btn-link btn-sm">
                                    <i class="material-icons">edit</i>
                                  </button>
                              </form>
                              <form  method="post" action="{{ route('pelicula.destroy',$pelicula->id) }}">
                                @csrf
                                @method('DELETE')
                              <button type="submit" rel="tooltip" title="Remove" class="btn btn-danger btn-link btn-sm">
                                <i class="material-icons">close</i>
                              </button>
                              </form>
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <!-- Paginacion -->
            <div class="row ">
              <div class="col-md-5 offset-md-4">
                <div class="clearfix">
                {{$peliculas->links()}}
              </div>
              </div>
            </div>
    
    </div>
    
@endsection
