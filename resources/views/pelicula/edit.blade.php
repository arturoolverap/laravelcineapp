@extends('layouts.admin')

@section('content')
  {{$errors}}

    <div class="container">
        <div class="row">
            
           
            <div class="col-md">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Pelicula</h4>
                  <p class="card-category">Complete los datos</p>
                </div>
                <div class="card-body">
                  <div class="card-body">
                   <form method="POST" action="{{ route('pelicula.update',$pelicula->id) }}" enctype="multipart/form-data">
                     @csrf
                     @method('PUT')
                    <div class="row">
                      <div class="col-md-5">
                        <div class="form-group">
                          <label class="bmd-label-floating">Titulo</label>
                          <input type="text" name="titulo" class="form-control"  value="{{$pelicula->titulo}}">
                          @error('titulo')
                            <span class="text-danger">{{ $message }}</span>
                          @enderror
                        </div>
                        
                        
                      </div>
                      <div class="col-md-5">
                        <div class="form-group">
                          <label class="bmd-label-floating">Duracion (Minutos)</label>
                          <input type="text" name="duracion" class="form-control"  value="{{$pelicula->duracion}}">
                          @error('duracion')
                            <span class="text-danger">{{ $message }}</span>
                          @enderror
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-5">
                          <div class="form-group">
                            <label for="exampleFormControlSelect1">Clasificación</label>
                            <select class="form-control" name="clasificacion" id="exampleFormControlSelect1">
                              <option value="AA">AA</option>
                              <option value="A">A</option>  
                              <option value="B">B</option>
                              <option value="B15">B15</option>
                              <option value="C">C</option>
                              <option value="D">D</option>
                            </select>
                        </div>
                      </div>
                      <div class="col-md-5">
                          <div class="form-group">
                            <label for="exampleFormControlSelect1">Genero</label>
                            <select class="form-control" name="genero" id="exampleFormControlSelect1">
                              <option value="Accion">Accion</option>
                              <option value="Aventura">Aventura </option>
                              <option value="Clasicas">Clasicas</option>
                              <option value="Comedia Romantica">Comedia Romantica</option>
                              <option value="Drama">Drama</option>
                              <option value="Terror">Terror</option>
                              <option value="Infantil">Infantil</option>
                              <option value="Accion y Aventura">Accion y Aventura</option>                  
                              <option value="Romantica">Romantica</option>                            
                            </select>
                        </div>
                      </div>
                  </div>
                   
                 
                  <div class="row">
                      <div class="col-md-5">
                        <div class="form-group">
                          <label class="bmd-label-floating">Fecha Estreno</label>
                          <input type="date" name="fechaEstreno" class="form-control"  value="{{ $pelicula->fechaEstreno}}">
                        </div>
                      </div>
                      <div class="col-md-5">
                          <div class="form-group">
                            <label for="exampleFormControlSelect1">Estatus</label>
                            <select class="form-control" name="estatus" id="exampleFormControlSelect1">
                              <option value="Activo">Activo</option>
                              <option value="Inactivo">Inactivo</option>                              
                            </select>
                        </div>
                      </div>

                  </div>
                  <br>
                  <h4>Detalles pelicula</h4><br>
                   <div class="row">
                      <div class="col-md-5">
                        <div class="form-group">
                          <label class="bmd-label-floating">Director</label>
                          <input type="text" name="director" class="form-control" value="{{$pelicula->detalles->director}}">
                          @error('director')
                              <span class="text-danger">{{ $message }}</span>
                          @enderror
                        </div>
                      </div>
                      <div class="col-md-5">
                        <div class="form-group">
                          <label class="bmd-label-floating">Actores</label>
                          <input type="text" name="actores" class="form-control"  value="{{$pelicula->detalles->actores}}">
                          @error('actores')
                              <span class="text-danger">{{ $message }}</span>
                          @enderror
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-5">
                        <div class="form-group">
                          <label class="bmd-label-floating">Sinopsis</label>
                          <input type="text" name="sinopsis" class="form-control"  value="{{ $pelicula->detalles->sinopsis}}">
                          @error('sinopsis')
                              <span class="text-danger">{{ $message }}</span>
                          @enderror
                        </div>
                      </div>
                      <div class="col-md-5">
                        <div class="form-group">
                          <label class="bmd-label-floating">Trailer</label>
                          <input type="text" name="trailer" class="form-control"  value="{{ $pelicula->detalles->trailer}}">
                          @error('trailer')
                              <span class="text-danger">{{ $message }}</span>
                          @enderror
                        </div>
                      </div>
                    </div>
                    
                    <button type="submit" class="btn btn-primary pull-right">Guardar</button>
                    <div class="clearfix"></div>
                  </form>
                </div>

            
        </div>
    </div>
    

     <script>
            $('#inputGroupFile02').on('change',function(){
                //get the file name
                var fileName = $(this).val();
                //replace the "Choose a file" label
                $(this).next('.custom-file-label').html(fileName);
            })
        </script>
@endsection
