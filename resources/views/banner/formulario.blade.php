@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            
           
            <div class="col-md">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Banner</h4>
                  <p class="card-category">Complete los datos</p>
                </div>
                <div class="card-body">
                  <form method="POST" action="{{ route('banner.store') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                      <div class="col-md-7">
                        <div class="form-group">
                          <label class="bmd-label-floating">Titulo</label>
                          <input type="text" name="titulo" class="form-control">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                        <div class="col-md-7">
                            <div class="input-group">
                              <div class="custom-file border rounded">
                                <input type="file" name="imagen" class="custom-file-input bg-primary " id="inputGroupFile02">
                                <label class="custom-file-label " for="inputGroupFile02">Elija imagen</label>
                              </div>
                      
                            </div>
                        </div>
                    </div>

                  <div class="row">
                      <div class="col-md-7">
                          <div class="form-group">
                            <label for="exampleFormControlSelect1">Estatus</label>
                            <select class="form-control" name="estatus" id="exampleFormControlSelect1">
                              <option value="Activo">Activo</option>
                              <option value="Inactivo">Inactivo</option>                              
                            </select>
                        </div>
                      </div>
                  </div>
                    
                    <button type="submit" class="btn btn-primary pull-right">Guardar</button>
                    <div class="clearfix"></div>
                  </form>
                </div>

            
        </div>
    </div>

     <script>
            $('#inputGroupFile02').on('change',function(){
                //get the file name
                var fileName = $(this).val();
                //replace the "Choose a file" label
                $(this).next('.custom-file-label').html(fileName);
            })
        </script>
@endsection
