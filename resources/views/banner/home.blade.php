@extends('layouts.admin')

@section('content')

  

    <div class="row">
      <div class="col-lg-3 col-md-6 col-sm-6">
           <div class="card card-stats">
                <div class="card-header card-header-success card-header-icon">
                  <div class="card-icon">
                    <a  style="color:white"  href="{{ url('/banner/create') }}">
                    <i class="material-icons">add</i>
                    </a>
                  </div>
                  <p class="card-category">Banner</p>
                  <h3 class="card-title">Nuevo</h3>
                  
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons"></i> 
                  </div>
                </div>
              </div>
             </div>

            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title ">Banner</h4>
                  <p class="card-category"> Here is a subtitle for this table</p>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table">
                      <thead class=" text-primary">
                        <th>ID</th>
                        <th>Titulo</th>
                        <th>Imagen</th>
                        <th>Estatus</th>
                        <th>Opciones</th>
                      </thead>
                      <tbody>

                        @foreach($banners as $banner)
                        <tr>
                          <td>{{$banner->id}}</td>
                          <td>{{$banner->titulo}}</td>
                          <td><img class="imagen" src="{{route('image',['filename'=>$banner->archivo])}}"></td>
                          <td>{{$banner->estatus}}</td>
                          <td class="td-actions text-left">
                            <form  method="post" action="{{ route('banner.update',$banner->id) }}">
                              @csrf
                              @method("PUT") 
                              <button type="submit" rel="tooltip" title="Desactiva" class="btn btn-danger btn-link btn-sm">
                                @if($banner->estatus == 'Activo')
                                  <i class="material-icons">close</i>
                                @else
                                  <i class="material-icons">done</i>
                                @endif
                              </button>
                               </form>
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <!-- Paginacion -->
            <div class="row ">
              <div class="col-md-5 offset-md-4">
                <div class="clearfix">
                {{$banners->links()}}
              </div>
              </div>
            </div>
    
    </div>
    <script>
    
    $(document).ready(function() {
        $('.active').click(function () {
            $.ajax({
            url : 'api/banner?page=1',
            dataType: 'html',
        }).done(function (data) {
          var json = $.parseJSON(data);
            $('tbody').html(recorrer(json.data));
            
            
        }).fail(function () {
            alert('data load failed.');
        });
        });
        $(document).on('click', '.pagination a', function (event) {
          
            getBlogs($(this).attr('href').split('page=')[1]);
            event.preventDefault();
        });
    });
    function getBlogs(page) {
        $.ajax({
            url : 'api/banner?page=' + page,
            dataType: 'html',
        }).done(function (data) {
          var json = $.parseJSON(data);
        
          $('tbody').html(recorrer(json.data));
            
            
        }).fail(function () {
            alert('data load failed.');
        });
    }
    function recorrer(data) {
      let salida;
      $.each(data,function(i, v) {
          salida += "<tr>";
          salida += "<td>"+v.id+"</td>";
          salida += "<td>"+v.titulo+"</td>";
          salida += "<td><img class='imagen' src='/image/file/"+v.archivo+"'></td>";
          salida += "<td>"+v.estatus+"</td>";
          salida +=  '<td class="td-actions text-left">';
          salida += "<form  method='post' action='banner/"+v.id+"'>";
          salida += '@csrf';
          salida += '@method("PUT")'; 
          salida += '<button type="submit" rel="tooltip" title="Remove" class="btn btn-danger btn-link btn-sm">';
          salida += ' <i class="material-icons">close</i>';
          salida += '</button>';
          salida += '</form>';
          salida += '</td>';
          
          // console.log( i , v);
      });
      return salida;
    }
    </script>
@endsection
