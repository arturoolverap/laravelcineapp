@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            
           
            <div class="col-md">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Usuario</h4>
                  <p class="card-category">Complete los datos</p>
                </div>
                <div class="card-body">
                  <form method="POST" action="{{ route('user.store') }}">
                    @csrf
                    <div class="row">
                      <div class="col-md-10">
                        <div class="form-group">
                          <label class="bmd-label-floating">Nombre Completo</label>
                          <input type="text" id="name" required name="name" class="form-control">
                        </div>
                      </div>
                    </div>
                     <div class="row">
                      <div class="col-md-10">
                        <div class="form-group">
                          <label class="bmd-label-floating">Nombre de usuario </label>
                          <input type="text" name="username" required class="form-control">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-10">
                        <div class="form-group">
                          <label class="bmd-label-floating">Email</label>
                          <input type="text" id="email" name="email" required class="form-control">
                        </div>
                      </div>
                    </div>
                   
                  <div class="row">
                      <div class="col-md-5">
                        <div class="form-group">
                          <label class="bmd-label-floating">Contraseña</label>
                          <input id="password" type="password" name="password" required class="form-control">
                        </div>
                      </div>
                      <div class="col-md-5">
                        <div class="form-group">
                          <label class="bmd-label-floating">Confirmación Contraseña</label>
                          <input id="password-confirm" type="password" name="password" required class="form-control">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-7">
                          <div class="form-group">
                            <label for="exampleFormControlSelect1">Rol</label>
                            <select class="form-control" name="role" id="exampleFormControlSelect1">
                              @foreach($roles as $role)
                                <option value="{{$role->id}}">{{$role->display_name}}</option>
                              @endforeach
                             
                                                          
                            </select>
                        </div>
                      </div>
                  </div>
                    
                    <button type="submit" class="btn btn-primary pull-right">Guardar</button>
                    <div class="clearfix"></div>
                  </form>
                </div>

            
        </div>
    </div>

     <script>
            $('#inputGroupFile02').on('change',function(){
                //get the file name
                var fileName = $(this).val();
                //replace the "Choose a file" label
                $(this).next('.custom-file-label').html(fileName);
            })
        </script>
@endsection
