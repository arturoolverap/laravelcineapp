@extends('layouts.admin')

@section('content')
  {{$errors}}

    <div class="container">
        <div class="row">
            
           
            <div class="col-md">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Horario</h4>
                  <p class="card-category">Complete los datos</p>
                </div>
                <div class="card-body">
                   <form method="POST" action="{{ route('horario.update',$horario->id) }}">
                     @csrf
                     @method('PUT')
                    
                      
                    
                 
                  <div class="row">
                      <div class="col-md-5">
                        <div class="form-group">
                          <label class="bmd-label-floating">Fecha</label>
                          <input type="date" name="fecha" class="form-control"  value="{{ $horario->fecha}}">
                        </div>
                      </div>
                     <div class="col-md-5">
                        <div class="form-group">
                          <label class="bmd-label-floating">Hora</label>
                          <input type="time" name="hora" class="form-control" value="{{ $horario->hora}}">
                          @error('hora')
                              <span class="text-danger">{{ $message }}</span>
                          @enderror
                        </div>
                      </div>

                  </div>
                   
                    <div class="row">
                      <div class="col-md-5">
                          <div class="form-group">
                            <label for="exampleFormControlSelect1">Sala</label>
                            <select class="form-control" name="sala" id="exampleFormControlSelect1">
                              <option value="Sala A">Sala A</option>
                              <option value="Sala B">Sala B</option>  
                              <option value="Sala C">Sala C</option>
                              <option value="Sala D">Sala D</option>
                              <option value="Sala Premium A">Sala Premium A</option>
                              <option value="Sala Premium B">Sala Premium B</option>
                            </select>
                        </div>
                      </div>
                      <div class="col-md-5">
                        <div class="form-group">
                          <label class="bmd-label-floating">precio</label>
                          <input type="number" name="precio" class="form-control"  value="{{ $horario->precio}}">
                          @error('precio')
                              <span class="text-danger">{{ $message }}</span>
                          @enderror
                        </div>
                      </div>
                    </div>
                    
                    <button type="submit" class="btn btn-primary pull-right">Guardar</button>
                    <div class="clearfix"></div>
                  </form>
                </div>

            
        </div>
    </div>
    

     <script>
            $('#inputGroupFile02').on('change',function(){
                //get the file name
                var fileName = $(this).val();
                //replace the "Choose a file" label
                $(this).next('.custom-file-label').html(fileName);
            })
        </script>
@endsection
