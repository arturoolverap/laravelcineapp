@extends('layouts.admin')

@section('content')

  

    <div class="row">
      <div class="col-lg-3 col-md-6 col-sm-6">
           <div class="card card-stats">
                <div class="card-header card-header-success card-header-icon">
                  <div class="card-icon">
                    <a  style="color:white"  href="{{ url('/pelicula/create') }}">
                    <i class="material-icons">add</i>
                    </a>
                  </div>
                  <p class="card-category">Horarios</p>
                  <h3 class="card-title">Nuevo</h3>
                  
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons"></i> 
                  </div>
                </div>
              </div>
             </div>

            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title ">Horarios</h4>
                  <p class="card-category"> Here is a subtitle for this table</p>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table">
                      <thead class=" text-primary">
                        <th>ID</th>
                        <th>Fecha</th>
                        <th>Hora</th>
                        <th>Sala</th>
                        <th>Precio</th>
                        <th>Pelicula</th>
                        <th>Opciones</th>
                      </thead>
                      <tbody>

                        @foreach($horarios as $horario)
                        <tr>
                          <td>{{$horario->id}}</td>
                          <td>{{$horario->fecha}}</td>
                          <td>{{$horario->hora}}</td>
                          <td>{{$horario->sala}}</td>  
                          <td>${{$horario->precio}}</td>  
                          <td>{{$horario->pelicula->titulo}}</td>  
                          <td class="td-actions text-left">
                             <form  method="get" action="{{ route('horario.edit',$horario->id) }}">
                                
                                <button type="submit" rel="tooltip" title="Edit Task" class="btn btn-primary btn-link btn-sm">
                                    <i class="material-icons">edit</i>
                                  </button>
                              </form>
                              <form  method="post" action="{{ route('horario.destroy',$horario->id) }}">
                                @csrf
                                @method('DELETE')
                              <button type="submit" rel="tooltip" title="Remove" class="btn btn-danger btn-link btn-sm">
                                <i class="material-icons">close</i>
                              </button>
                              </form>
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <!-- Paginacion -->
            <div class="row ">
              <div class="col-md-5 offset-md-4">
                <div class="clearfix">
                {{$horarios->links()}}
              </div>
              </div>
            </div>
    
    </div>
    
@endsection
